<?php
// 1.7.* Üstü İçin Yazılmıştır
$serverip = isset($_GET['s']) ? $_GET['s'] : 'localhost';

function get_data($url)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 1);
    $data       = curl_exec($ch);
    $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    return array(
        'status' => $httpStatus,
        'data' => $data
    );
} // Get Data and Status
$usersinserver = get_data("http://api.iamphoenix.me/list/?server_ip=" . $serverip . "");
$serverdata    = get_data("http://minecraft-api.com/v1/get/?server=" . $serverip . "");
$data_list     = json_decode($usersinserver["data"], true);
$data_general  = json_decode($serverdata["data"], true);
$array_list    = explode(',', $data_list['players']);
?>
<!DOCTYPE html>
<html>
	<head>
        <meta charset="utf-8">
        <title>Minecraft Server Bilgisi</title>
        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css">
    	<link href='http://fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' type='text/css'>
    	<link href="https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    	<script type="text/javascript" src="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
    	<script language="javascript">
   		jQuery(document).ready(function(){
			$("[rel='tooltip']").tooltip();
    	});
		</script>
    	<style>
    	body {
      		font-family: 'Lato', sans-serif !important;
    	}
    	</style>
    </head>
    <body>
	<div class="container">
        <h1>MC PHP Query</h1><hr>       
		<div class="row">
			<div class="span4">
				<h3>Genel Bilgiler</h3>
				<table class="table table-striped">
					<tbody>
					<tr>
					<td><b>IP</b></td>
					<td><?php echo $serverip; ?></td>
					</tr>
					<?php if ($serverdata["status"] == "200" && $data_general['error'] == "") { ?><tr>
					<td><b>Version</b></td>
					<td><?php echo $data_general['version']; ?></td> 
					</tr><?php } ?>
					<?php if ($serverdata["status"] == "200" && $data_general['error'] == "") { ?><tr>
					<td><b>Kullanıcılar</b></td>
					<td><?php echo "" . $data_general['players']['online'] . " / " . $data_general['players']['max'] . "";?></td>
					</tr><?php } ?>
					<tr>
					<td><b>Durum</b></td>
					<td><? if ($data_general['status'] == 'true') {echo "<i class=\"icon-ok-sign\"></i> Server açık";} else {echo "<i class=\"icon-remove-sign\"></i> Server kapalı";}?></td>
					</tr>
					<?php if ($serverdata["status"] == "200" && $data_general['error'] == "") {?><tr>
					<td><b>Ping</b></td>
					<td><?php echo "" . $data_general['latency'] . "ms"; ?></td>
					</tr><?php } ?>		
					</tbody>
				</table>
			</div>
			<div class="span8">
				<h3>Kullanıcılar</h3>
				<?php
if ($usersinserver["status"] == "200" && $data_general['error'] == "") {
    foreach ($array_list as $key => $value) {
        $users .= "<a data-placement=\"top\" rel=\"tooltip\" style=\"display: inline-block;\" title=\"" . $value . "\">
				<img src=\"https://minotar.net/avatar/" . $value . "/50\" size=\"40\" width=\"40\" height=\"40\" style=\"width: 40px; height: 40px; margin-bottom: 5px; margin-right: 5px; border-radius: 3px;\"/></a>";
    }
    if ($data_general['players']['online'] > 0) {
        print_r($users);
    } else {
        echo "<div class=\"alert\"> Şuan serverde kullanıcı yok</div>";
    }
} else {
    echo "<div class=\"alert alert-danger\"> Bu Server Kullanıcı Listesini Göstermiyor</div>";
}
?>
			</div>
		</div>
	</div>
	</body>
</html>
